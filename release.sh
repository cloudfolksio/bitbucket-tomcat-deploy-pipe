#!/usr/bin/env bash

set -ex
IMAGE=$1

## Generate new version

previous_version=$(semversioner current-version)
semversioner release
new_version=$(semversioner current-version)

## Generate CHANGELOG.md

echo "Generating CHANGELOG.md file..."
semversioner changelog > CHANGELOG.md

# Use new version in the README.md examples
echo "Replace version '$previous_version' to '$new_version' in README.md ..."
sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" README.md

# Use new version in the pipe.yml metadata file
echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" pipe.yml

## Build and push docker image

echo "Build and push docker image..."
echo ${DOCKERHUB_TOKEN} | docker login --username "$DOCKERHUB_USER" --password-stdin
docker build -t ${IMAGE}:${new_version} .
docker tag ${IMAGE}:${new_version} ${IMAGE}:latest
docker push ${IMAGE}:${new_version}
docker push ${IMAGE}:latest

## Commit back to the repository

echo "Committing updated files to the repository..."
git add .
git commit -m "[skip ci] Updates for '${new_version}'"
git push origin master

## Tag the repository

echo "Tag release ${new_version}"
git tag -a -m "Tag release ${new_version}" "${new_version}"
git push origin ${new_version}
