FROM alpine:3.11

RUN apk add --update --no-cache curl
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.5.0/common.sh

COPY LICENSE deploy.sh /

ENTRYPOINT ["/deploy.sh"]
