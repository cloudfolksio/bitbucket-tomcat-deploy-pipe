#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="radekantoniuk/bitbucket-tomcat-deploy-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Can't open file test" {
    run docker run --rm \
        -e DEPLOY_USER="user" \
        -e DEPLOY_PASS="url" \
        -e DEPLOY_URL="url" \
        -e DEPLOY_FILE="file" \
        -e DEPLOY_PATH="/deploy" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "OK scenario" {
    run docker run \
        --name tomcat_pipe_test \
        -e DEPLOY_USER="user" \
        -e DEPLOY_PASS="url" \
        -e DEPLOY_URL="url" \
        -e DEPLOY_FILE="app.war" \
        -e DEPLOY_PATH="/deploy" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test \
        mkdir -p /opt/atlassian/pipelines/agent/build/ && \
        echo " " > /opt/atlassian/pipelines/agent/build/app.war

        run docker start tomcat_pipe_test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}
