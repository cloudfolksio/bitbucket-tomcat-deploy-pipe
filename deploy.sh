#!/bin/sh
#set -x
set -e

RETRIES=${RETRIES:-60}
DEPLOY_USER=${DEPLOY_USER:?'DEPLOY_USER variable missing.'}
DEPLOY_PASS=${DEPLOY_PASS:?'DEPLOY_PASS variable missing.'}
DEPLOY_URL=${DEPLOY_URL:?'DEPLOY_URL variable missing.'}
DEPLOY_FILE=${DEPLOY_FILE:?'DEPLOY_FILE variable missing.'}
DEPLOY_PATH=${DEPLOY_PATH:?'DEPLOY_PATH variable missing.'}
DEPLOY_FILE=/opt/atlassian/pipelines/agent/build/$DEPLOY_FILE

count=0

check_command() {
  curl --silent --user $DEPLOY_USER:$DEPLOY_PASS $DEPLOY_URL/list | grep -q $DEPLOY_PATH:running
}

deploy_command() {
  curl --silent --show-error --fail --user $DEPLOY_USER:$DEPLOY_PASS --upload-file $DEPLOY_FILE "$DEPLOY_URL/deploy?path=$DEPLOY_PATH&update=true"

}

if [ ! -f $DEPLOY_FILE ]; then
  echo "Can't open $DEPLOY_FILE";
  exit 1;
fi

deploy_command

echo -n "Checking for application running status "

until check_command
do

  echo -n "."
  count=$((count+1))

  if [ "$count" -gt "$RETRIES" ]; then
    echo
    echo "[ERR] Timeout"
    exit 1
  fi
  
  sleep 1
done

echo " done!"
