# Bitbucket Pipelines Pipe: Tomcat deploy

Deploy an artifact (e.g. ZIP,WAR) using Tomcat Manager.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:           

```yaml
- pipe: docker://cloudfolksio/bitbucket-tomcat-deploy-pipe:1.3.0
  variables:
    DEPLOY_URL: '<string>'
    DEPLOY_USER: '<string>'
    DEPLOY_PASS: '<string>'
    DEPLOY_FILE: '<string>'
    DEPLOY_PATH: '<string>'
    RETRIES: 60 # Optional
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| DEPLOY_URL (*)                       | Path to Tomcat Manager.  |
| DEPLOY_USER (*)                      | Tomcat user that has privileges to use Tomcat Manager app. |
| DEPLOY_PASS (*)                      | Password for the user above. |
| DEPLOY_FILE (*)               | The artifact filename saved from previous pipeline steps. |
| DEPLOY_PATH (*)                | Application name (Tomcat context path) that the app will be visible under. |
| RETRIES                       | How many seconds should the script wait for the app to become in _running_ state (default 60).   |

_(*) = required variable._


## Prerequisites

You need to configure Tomcat Manager with a user that has *manager-script* role, example of _conf/tomcat-users.xml_:


```xml
<tomcat-users xmlns="http://tomcat.apache.org/xml"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd"
              version="1.0">
<role rolename="manager-script" />
<user username="deploy" password="MyPassword" roles="manager-script"/>
</tomcat-users>
```

## Examples

### Basic example:

```yaml

- step:
    name: Build
    caches:
      - maven
    script:
      - mvn -B clean package
    artifacts:
      - target/**.war

# the next step assumes that appname.war was archived during the build step above

- step:
    name: Deploy to Tomcat
    deployment: dev
    script:
      - pipe: docker://cloudfolksio/bitbucket-tomcat-deploy-pipe:1.3.0
        variables:
          DEPLOY_URL: 'http://localhost:8080/manager/text'
          DEPLOY_USER: 'deploy'
          DEPLOY_PASS: 'password'
          DEPLOY_FILE: appname.war
          DEPLOY_PATH: /appname
```

### Example using repository variables


```yaml
- pipe: docker://cloudfolksio/bitbucket-tomcat-deploy-pipe:1.3.0
  variables:
    DEPLOY_URL: $TOMCAT_URL
    DEPLOY_USER: $TOMCAT_USER
    DEPLOY_PASS: $TOMCAT_PASS
    DEPLOY_FILE: appname.war
    DEPLOY_PATH: /appname
    RETRIES: 60 # Optional
```

The **$TOMCAT_URL, $TOMCAT_USER and $TOMCAT_PASS** are variables defined per environment in the Deployment repository settings.


## Releasing new version


* prepare the Changelog on *develop* branch:

```bash
$ docker run --rm -it -w /app -v $PWD:/app warden/semversioner add-change --type patch --description "Test patch release"
```

- open Pull Request from *develop* to *master*, when it is merged BitBucket Pipelines will do the rest

## Support

If you'd like help with this pipe, or you have an issue or feature request, feel free to Open a Pull Request or [open an issue](https://bitbucket.org/radekantoniuk/bitbucket-tomcat-deploy-pipe/issues).

When reporting an issue, remember to include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce
