# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.3.0

- minor: Move to cloudfolksio in Docker Hub

## 1.2.0

- minor: Rename variables USER, PASS, URL to DEPLOY_USER, DEPLOY_PASS, DEPLOY_URL respectively, to avoid clashes with shell variables
- patch: Fix for application not being updated when already deployed

## 1.1.0

- minor: Minor prettyfying and adding test

## 1.0.0

- major: Initial release

